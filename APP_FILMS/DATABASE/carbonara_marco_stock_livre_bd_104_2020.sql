-- MC 2020.02.12
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: carbonara_marco_stock_livre_bd_104_2020

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS carbonara_marco_stock_livre_bd_104_2020;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS carbonara_marco_stock_livre_bd_104_2020;

-- Utilisation de cette base de donnée

USE carbonara_marco_stock_livre_bd_104_2020;
-- --------------------------------------------------------

-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 01 Mai 2020 à 15:14
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_auteur`
--

CREATE TABLE `t_auteur` (
  `id_Auteur` int(11) NOT NULL,
  `Nom_Auteur` varchar(12) NOT NULL,
  `Date_Auteur` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_auteur`
--

INSERT INTO `t_auteur` (`id_Auteur`, `Nom_Auteur`, `Date_Auteur`) VALUES
(1, 'Bonjar', '2020-03-03'),
(2, 'HP Lovecraft', '2020-04-06'),
(3, 'JK Rowling', '2020-04-06'),
(4, 'Boris Vian', '2020-04-29'),
(5, 'Kamel', '2020-04-29'),
(6, 'Maskey', '2020-04-29'),
(8, 'je vous aime', '0000-00-00'),
(9, 'je vous aime', '0000-00-00'),
(10, 'je vous aime', '0000-00-00'),
(11, 'je vous aime', '0000-00-00'),
(12, 'je vous aime', '0000-00-00'),
(14, 'je vous aime', '0000-00-00'),
(15, 'je vous aime', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `t_books`
--

CREATE TABLE `t_books` (
  `id_Books` int(11) NOT NULL,
  `Isbn_Books` int(11) NOT NULL,
  `Titre_Books` varchar(22) NOT NULL,
  `Country_ Books` varchar(12) NOT NULL,
  `Date_Books` date NOT NULL,
  `Publieur_Books` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books`
--

INSERT INTO `t_books` (`id_Books`, `Isbn_Books`, `Titre_Books`, `Country_ Books`, `Date_Books`, `Publieur_Books`) VALUES
(2, 1004, 'Skurrt', 'Swiss', '2020-03-03', 'Baptispe'),
(3, 200002, 'The Call of Cthulhu', 'UnitedStates', '2020-04-06', 'the pulp magazine Weird Tales'),
(4, 1869, 'Ma vie', 'Belgique', '2013-03-28', 'LE CHERCHE MIDI'),
(5, 1234, 'Life', 'France', '2019-10-16', 'Glénat'),
(6, 1003, 'LesAnimauxfantastiques', 'royaume uni', '2001-03-12', 'Bloomsbury'),
(8, 0, 'je vous aime ', '', '0000-00-00', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_auteur`
--

CREATE TABLE `t_books_a_auteur` (
  `id_Books_A_Auteur` int(11) NOT NULL,
  `FK_Auteur` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `Date_Books_A_Auteur` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_auteur`
--

INSERT INTO `t_books_a_auteur` (`id_Books_A_Auteur`, `FK_Auteur`, `FK_Books`, `Date_Books_A_Auteur`) VALUES
(1, 2, 3, '2020-04-28 22:00:00'),
(2, 5, 4, '2020-04-21 22:00:00'),
(3, 6, 5, '2020-04-28 22:00:00'),
(4, 3, 6, '2020-04-28 22:00:00'),
(5, 4, 2, '2020-04-28 22:00:00'),
(6, 4, 3, '2020-04-29 22:00:00'),
(7, 4, 3, '2020-04-30 16:00:32'),
(8, 6, 8, '2020-04-30 16:16:57');

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_format`
--

CREATE TABLE `t_books_a_format` (
  `id_Books_A_Format` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Format` int(11) NOT NULL,
  `Date_Books_A_Format` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_format`
--

INSERT INTO `t_books_a_format` (`id_Books_A_Format`, `FK_Books`, `FK_Format`, `Date_Books_A_Format`) VALUES
(1, 2, 1, '2020-04-28 22:00:00'),
(2, 6, 2, '2020-04-28 22:00:00'),
(3, 4, 3, '2020-04-28 22:00:00'),
(4, 5, 4, '2020-04-28 22:00:00'),
(5, 3, 5, '2020-04-28 22:00:00'),
(6, 2, 6, '2020-04-28 22:00:00'),
(7, 3, 4, '2020-04-29 22:00:00'),
(8, 3, 4, '2020-04-30 16:16:06'),
(9, 3, 4, '2020-04-30 16:18:05'),
(10, 3, 4, '2020-04-30 16:30:01');

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_genres`
--

CREATE TABLE `t_books_a_genres` (
  `id_Books_A_Genres` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Genres` int(11) NOT NULL,
  `Date_Books_A_Genres` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_genres`
--

INSERT INTO `t_books_a_genres` (`id_Books_A_Genres`, `FK_Books`, `FK_Genres`, `Date_Books_A_Genres`) VALUES
(1, 6, 2, '2020-04-28 22:00:00'),
(2, 2, 1, '2020-04-28 22:00:00'),
(3, 3, 3, '2020-04-28 22:00:00'),
(4, 4, 4, '2020-04-28 22:00:00'),
(5, 3, 4, '2020-04-29 22:00:00'),
(6, 3, 4, '2020-04-30 16:21:32'),
(7, 3, 4, '2020-04-30 16:24:28'),
(8, 3, 4, '2020-04-30 16:29:55'),
(9, 3, 4, '2020-04-30 16:42:19');

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_illustrateur`
--

CREATE TABLE `t_books_a_illustrateur` (
  `id_Books_A_Illustrateur` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Illustrateur` int(11) NOT NULL,
  `Date_Books_A_Illustrateur` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_illustrateur`
--

INSERT INTO `t_books_a_illustrateur` (`id_Books_A_Illustrateur`, `FK_Books`, `FK_Illustrateur`, `Date_Books_A_Illustrateur`) VALUES
(1, 2, 5, '2020-04-28 22:00:00'),
(2, 3, 4, '2020-04-28 22:00:00'),
(3, 4, 3, '2020-04-28 22:00:00'),
(4, 5, 2, '2020-04-28 22:00:00'),
(5, 6, 1, '2020-04-28 22:00:00'),
(6, 3, 4, '2020-04-30 16:46:58');

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_langue`
--

CREATE TABLE `t_books_a_langue` (
  `id_Books_A_Langue` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Langue` int(11) NOT NULL,
  `Date_Books_A_Langue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_langue`
--

INSERT INTO `t_books_a_langue` (`id_Books_A_Langue`, `FK_Books`, `FK_Langue`, `Date_Books_A_Langue`) VALUES
(1, 2, 4, '2020-04-28 22:00:00'),
(2, 3, 2, '2020-04-28 22:00:00'),
(3, 4, 3, '2020-04-28 22:00:00'),
(4, 5, 3, '2020-04-28 22:00:00'),
(5, 6, 2, '2020-04-28 22:00:00'),
(6, 3, 4, '2020-04-29 22:00:00'),
(7, 3, 4, '2020-04-30 16:43:29');

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_page`
--

CREATE TABLE `t_books_a_page` (
  `id_Books_A_Page` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Page` int(11) NOT NULL,
  `Date_Books_A_Page` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_page`
--

INSERT INTO `t_books_a_page` (`id_Books_A_Page`, `FK_Books`, `FK_Page`, `Date_Books_A_Page`) VALUES
(1, 2, 2, '2020-04-28 22:00:00'),
(2, 5, 3, '2020-04-28 22:00:00'),
(3, 4, 4, '2020-04-29 22:00:00'),
(4, 3, 5, '2020-04-28 22:00:00'),
(5, 6, 6, '2020-04-28 22:00:00'),
(6, 3, 4, '2020-04-29 22:00:00'),
(7, 3, 4, '2020-04-30 16:49:34');

-- --------------------------------------------------------

--
-- Structure de la table `t_format`
--

CREATE TABLE `t_format` (
  `id_Format` int(11) NOT NULL,
  `Nom_Format` varchar(11) NOT NULL,
  `Edition_Format` varchar(22) NOT NULL,
  `Date_Format` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_format`
--

INSERT INTO `t_format` (`id_Format`, `Nom_Format`, `Edition_Format`, `Date_Format`) VALUES
(1, 'A7', 'KobalaD', '2020-03-09'),
(2, 'Roman', 'Bloomsbury', '2020-04-29'),
(3, 'BIOGRAPHIES', 'LE CHERCHE MIDI', '2020-04-29'),
(4, 'BD', 'Glénat', '2020-04-29'),
(5, 'Policier', 'pulpmagazineWeirdTales', '2020-04-29'),
(6, 'Brochure', 'Baptispe', '2020-04-29'),
(9, 'je vous aim', '', '0000-00-00'),
(10, 'je vous aim', '', '0000-00-00'),
(11, 'je vous aim', '', '0000-00-00'),
(12, 'je vous aim', '', '0000-00-00'),
(13, 'je vous aim', '', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `t_genres`
--

CREATE TABLE `t_genres` (
  `Id_Genres` int(11) NOT NULL,
  `Nom_Genres` varchar(9) NOT NULL,
  `Date Genres` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_genres`
--

INSERT INTO `t_genres` (`Id_Genres`, `Nom_Genres`, `Date Genres`) VALUES
(1, 'Smirnoff', '2020-03-10'),
(2, 'Fantasy', '2020-04-29'),
(3, 'SF', '2020-04-29'),
(4, 'bio', '2020-04-29'),
(5, 'Humour', '2020-04-29');

-- --------------------------------------------------------

--
-- Structure de la table `t_illustrateur`
--

CREATE TABLE `t_illustrateur` (
  `id_Illustrateur` int(11) NOT NULL,
  `Nom_Illustrateur` varchar(11) NOT NULL,
  `Date_Illustrateur` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_illustrateur`
--

INSERT INTO `t_illustrateur` (`id_Illustrateur`, `Nom_Illustrateur`, `Date_Illustrateur`) VALUES
(1, 'Baka', '2020-03-10'),
(2, 'Didier', '2020-04-29'),
(3, 'PascalLeGFr', '2020-04-29'),
(4, 'MahdiBa', '2020-04-29'),
(5, 'clowneriea', '2020-04-29'),
(7, 'je vous aim', '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `t_langue`
--

CREATE TABLE `t_langue` (
  `Id_Langue` int(11) NOT NULL,
  `Dispo_Langue` varchar(11) NOT NULL,
  `Date_Langue` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_langue`
--

INSERT INTO `t_langue` (`Id_Langue`, `Dispo_Langue`, `Date_Langue`) VALUES
(2, 'Anglais', '2020-04-29'),
(3, 'Français', '2020-04-29'),
(4, 'Latin', '2020-04-29');

-- --------------------------------------------------------

--
-- Structure de la table `t_page`
--

CREATE TABLE `t_page` (
  `Id_Page` int(11) NOT NULL,
  `No_Page` int(11) NOT NULL,
  `Date_Page` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_page`
--

INSERT INTO `t_page` (`Id_Page`, `No_Page`, `Date_Page`) VALUES
(2, 1467, '2020-04-29'),
(3, 48, '2020-04-29'),
(4, 175, '2020-04-29'),
(5, 776, '2020-04-29'),
(6, 96, '2020-04-29'),
(9, 56, '0000-00-00');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_auteur`
--
ALTER TABLE `t_auteur`
  ADD PRIMARY KEY (`id_Auteur`);

--
-- Index pour la table `t_books`
--
ALTER TABLE `t_books`
  ADD PRIMARY KEY (`id_Books`);

--
-- Index pour la table `t_books_a_auteur`
--
ALTER TABLE `t_books_a_auteur`
  ADD PRIMARY KEY (`id_Books_A_Auteur`),
  ADD KEY `FK_Auteur` (`FK_Auteur`),
  ADD KEY `FK_Books` (`FK_Books`);

--
-- Index pour la table `t_books_a_format`
--
ALTER TABLE `t_books_a_format`
  ADD PRIMARY KEY (`id_Books_A_Format`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Format` (`FK_Format`);

--
-- Index pour la table `t_books_a_genres`
--
ALTER TABLE `t_books_a_genres`
  ADD PRIMARY KEY (`id_Books_A_Genres`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Genres` (`FK_Genres`);

--
-- Index pour la table `t_books_a_illustrateur`
--
ALTER TABLE `t_books_a_illustrateur`
  ADD PRIMARY KEY (`id_Books_A_Illustrateur`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Illustrateur` (`FK_Illustrateur`);

--
-- Index pour la table `t_books_a_langue`
--
ALTER TABLE `t_books_a_langue`
  ADD PRIMARY KEY (`id_Books_A_Langue`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Langue` (`FK_Langue`);

--
-- Index pour la table `t_books_a_page`
--
ALTER TABLE `t_books_a_page`
  ADD PRIMARY KEY (`id_Books_A_Page`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Page` (`FK_Page`);

--
-- Index pour la table `t_format`
--
ALTER TABLE `t_format`
  ADD PRIMARY KEY (`id_Format`);

--
-- Index pour la table `t_genres`
--
ALTER TABLE `t_genres`
  ADD PRIMARY KEY (`Id_Genres`);

--
-- Index pour la table `t_illustrateur`
--
ALTER TABLE `t_illustrateur`
  ADD PRIMARY KEY (`id_Illustrateur`);

--
-- Index pour la table `t_langue`
--
ALTER TABLE `t_langue`
  ADD PRIMARY KEY (`Id_Langue`);

--
-- Index pour la table `t_page`
--
ALTER TABLE `t_page`
  ADD PRIMARY KEY (`Id_Page`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_auteur`
--
ALTER TABLE `t_auteur`
  MODIFY `id_Auteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `t_books`
--
ALTER TABLE `t_books`
  MODIFY `id_Books` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_books_a_auteur`
--
ALTER TABLE `t_books_a_auteur`
  MODIFY `id_Books_A_Auteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `t_books_a_format`
--
ALTER TABLE `t_books_a_format`
  MODIFY `id_Books_A_Format` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `t_books_a_genres`
--
ALTER TABLE `t_books_a_genres`
  MODIFY `id_Books_A_Genres` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `t_books_a_illustrateur`
--
ALTER TABLE `t_books_a_illustrateur`
  MODIFY `id_Books_A_Illustrateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `t_books_a_langue`
--
ALTER TABLE `t_books_a_langue`
  MODIFY `id_Books_A_Langue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_books_a_page`
--
ALTER TABLE `t_books_a_page`
  MODIFY `id_Books_A_Page` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_format`
--
ALTER TABLE `t_format`
  MODIFY `id_Format` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `t_genres`
--
ALTER TABLE `t_genres`
  MODIFY `Id_Genres` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `t_illustrateur`
--
ALTER TABLE `t_illustrateur`
  MODIFY `id_Illustrateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_langue`
--
ALTER TABLE `t_langue`
  MODIFY `Id_Langue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_page`
--
ALTER TABLE `t_page`
  MODIFY `Id_Page` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_books_a_auteur`
--
ALTER TABLE `t_books_a_auteur`
  ADD CONSTRAINT `t_books_a_auteur_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_auteur_ibfk_2` FOREIGN KEY (`FK_Auteur`) REFERENCES `t_auteur` (`id_Auteur`);

--
-- Contraintes pour la table `t_books_a_format`
--
ALTER TABLE `t_books_a_format`
  ADD CONSTRAINT `t_books_a_format_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_format_ibfk_2` FOREIGN KEY (`FK_Format`) REFERENCES `t_format` (`id_Format`);

--
-- Contraintes pour la table `t_books_a_genres`
--
ALTER TABLE `t_books_a_genres`
  ADD CONSTRAINT `t_books_a_genres_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_genres_ibfk_2` FOREIGN KEY (`FK_Genres`) REFERENCES `t_genres` (`Id_Genres`);

--
-- Contraintes pour la table `t_books_a_illustrateur`
--
ALTER TABLE `t_books_a_illustrateur`
  ADD CONSTRAINT `t_books_a_illustrateur_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_illustrateur_ibfk_2` FOREIGN KEY (`FK_Illustrateur`) REFERENCES `t_illustrateur` (`id_Illustrateur`);

--
-- Contraintes pour la table `t_books_a_langue`
--
ALTER TABLE `t_books_a_langue`
  ADD CONSTRAINT `t_books_a_langue_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_langue_ibfk_2` FOREIGN KEY (`FK_Langue`) REFERENCES `t_langue` (`Id_Langue`);

--
-- Contraintes pour la table `t_books_a_page`
--
ALTER TABLE `t_books_a_page`
  ADD CONSTRAINT `t_books_a_page_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_page_ibfk_2` FOREIGN KEY (`FK_Page`) REFERENCES `t_page` (`Id_Page`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
