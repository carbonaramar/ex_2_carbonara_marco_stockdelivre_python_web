# routes_gestion_t_langue.py
# OM 2020.04.06 Gestions des "routes" FLASK pour les langue.

from flask import render_template, flash, redirect, url_for, request
from APP_FILMS import obj_mon_application
from APP_FILMS.t_langue.data_gestion_t_langue import Gestiont_langue
from APP_FILMS.DATABASE.erreurs import *
# OM 2020.04.10 Pour utiliser les expressions régulières REGEX
import re


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /t_langue_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/t_langue_afficher
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/t_langue_afficher", methods=['GET', 'POST'])
def t_langue_afficher():
    # OM 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs du formulaire HTML.
    if request.method == "GET":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_t_langue = Gestiont_langue()
            # Récupére les données grâce à une requête MySql définie dans la classe Gestiont_langue()
            # Fichier data_gestion_t_langue.py
            data_t_langue = obj_actions_t_langue.t_langue_afficher_data()
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(" data t_langue", data_t_langue, "type ", type(data_t_langue))

            # OM 2020.04.09 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
            flash("Données t_langue affichées !!", "Success")
        except Exception as erreur:
            print(f"RGG Erreur générale.")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            # flash(f"RGG Exception {erreur}")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("t_langue/t_langue_afficher.html", data=data_t_langue)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /t_langue_add ,
# cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template"
# En cas d'erreur on affiche à nouveau la page "t_langue_add.html"
# Pour la tester http://127.0.0.1:1234/t_langue_add
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/t_langue_add", methods=['GET', 'POST'])
def t_langue_add():
    # OM 2019.03.25 Pour savoir si les données d'un formulaire sont un affichage
    # ou un envoi de donnée par des champs utilisateurs.
    if request.method == "POST":
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_t_langue = Gestiont_langue()
            # OM 2020.04.09 Récupère le contenu du champ dans le formulaire HTML "t_langue_add.html"
            name_langue = request.form['name_langue_html']

            # OM 2019.04.04 On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$",
                                name_langue):
                # OM 2019.03.28 Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")
                # On doit afficher à nouveau le formulaire "t_langue_add.html" à cause des erreurs de "claviotage"
                return render_template("t_langue/t_langue_add.html")
            else:

                # Constitution d'un dictionnaire et insertion dans la BD
                valeurs_insertion_dictionnaire = {"value_Dispo_Langue": name_langue}
                obj_actions_t_langue.add_langue._data(valeurs_insertion_dictionnaire)

                # OM 2019.03.25 Les 2 lignes ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Données insérées !!", "Sucess")
                print(f"Données insérées !!")
                # On va interpréter la "route" 't_langue_afficher', car l'utilisateur
                # doit voir le nouveau langue qu'il vient d'insérer.
                return redirect(url_for('t_langue_afficher'))

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur:
            # OM 2020.04.09 On dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurDoublon(f"RGG pei {msg_erreurs['ErreurDoublonValue']['message']} et son status {msg_erreurs['ErreurDoublonValue']['status']}")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur:
            flash(f"Autre erreur {erreur}")
            raise MonErreur(f"Autre erreur")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts très important de respecter l'ordre.
        except Exception as erreur:
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']} et son status {msg_erreurs['ErreurConnexionBD']['status']}")
    # OM 2020.04.07 Envoie la page "HTML" au serveur.
    return render_template("t_langue/t_langue_add.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /t_langue_edit , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un langue de books par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/t_langue_edit', methods=['POST', 'GET'])
def t_langue_edit():
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "t_langue_afficher.html"
    if request.method == 'GET':
        try:
            # Récupérer la valeur de "Id_Langue" du formulaire html "t_langue_afficher.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "Id_Langue"
            # grâce à la variable "Id_Langue_edit_html"
            # <a href="{{ url_for('t_langue_edit', Id_Langue_edit_html=row.Id_Langue) }}">Edit</a>
            Id_Langue_edit = request.values['Id_Langue_edit_html']

            # Pour afficher dans la console la valeur de "Id_Langue_edit", une façon simple de se rassurer,
            # sans utiliser le DEBUGGER
            print(Id_Langue_edit)

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_select_dictionnaire = {"value_Id_Langue": Id_Langue_edit}

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_t_langue = Gestiont_langue()

            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_Id_Langue = obj_actions_t_langue.edit_langue_data(valeur_select_dictionnaire)
            print("dataIdlangue ", data_Id_Langue, "type ", type(data_Id_Langue))
            # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
            flash(f"Editer le langue d'un books !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            # On indique un problème, mais on ne dit rien en ce qui concerne la résolution.
            print("Problème avec la BD ! : %s", erreur)
            # OM 2020.04.09 On dérive "Exception" dans "MaBdErreurConnexion" fichier "erreurs.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            raise MaBdErreurConnexion(f"RGG Exception {msg_erreurs['ErreurConnexionBD']['message']}"
                                      f"et son status {msg_erreurs['ErreurConnexionBD']['status']}")

    return render_template("t_langue/t_langue_edit.html", data=data_Id_Langue)


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /t_langue_update , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un langue de books par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/t_langue_update', methods=['POST', 'GET'])
def t_langue_update():
    # DEBUG bon marché : Pour afficher les méthodes et autres de la classe "flask.request"
    print(dir(request))
    # OM 2020.04.07 Les données sont affichées dans un formulaire, l'affichage de la sélection
    # d'une seule ligne choisie par le bouton "edit" dans le formulaire "t_langue_afficher.html"
    # Une fois que l'utilisateur à modifié la valeur du langue alors il va appuyer sur le bouton "UPDATE"
    # donc en "POST"
    if request.method == 'POST':
        try:
            # DEBUG bon marché : Pour afficher les valeurs contenues dans le formulaire
            print("request.values ",request.values)

            # Récupérer la valeur de "Id_Langue" du formulaire html "t_langue_edit.html"
            # l'utilisateur clique sur le lien "edit" et on récupére la valeur de "Id_Langue"
            # grâce à la variable "Id_Langue_edit_html"
            # <a href="{{ url_for('t_langue_edit', Id_Langue_edit_html=row.Id_Langue) }}">Edit</a>
            Id_Langue_edit = request.values['Id_Langue_edit_html']

            # Récupère le contenu du champ "Dispo_Langue" dans le formulaire HTML "t_langueEdit.html"
            name_langue = request.values['name_edit_Dispo_Langue_html']
            valeur_edit_list = [{'Id_Langue': Id_Langue_edit, 'Dispo_Langue': name_langue}]
            # On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
            # des valeurs avec des caractères qui ne sont pas des lettres.
            # Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
            if not re.match("^([A-Z]|[a-z\u00C0-\u00FF])[A-Za-z\u00C0-\u00FF]*['\\- ]?[A-Za-z\u00C0-\u00FF]+$", name_langue):
                # En cas d'erreur, conserve la saisie fausse, afin que l'utilisateur constate sa misérable faute
                # Récupère le contenu du champ "Dispo_Langue" dans le formulaire HTML "t_langueEdit.html"
                #name_langue = request.values['name_edit_Dispo_Langue_html']
                # Message humiliant à l'attention de l'utilisateur.
                flash(f"Une entrée...incorrecte !! Pas de chiffres, de caractères spéciaux, d'espace à double, "
                      f"de double apostrophe, de double trait union et ne doit pas être vide.", "Danger")

                # On doit afficher à nouveau le formulaire "t_langue_edit.html" à cause des erreurs de "claviotage"
                # Constitution d'une liste pour que le formulaire d'édition "t_langue_edit.html" affiche à nouveau
                # la possibilité de modifier l'entrée
                # Exemple d'une liste : [{'Id_Langue': 13, 'Dispo_Langue': 'philosophique'}]
                valeur_edit_list = [{'Id_Langue': Id_Langue_edit, 'Dispo_Langue': name_langue}]

                # DEBUG bon marché :
                # Pour afficher le contenu et le type de valeurs passées au formulaire "t_langue_edit.html"
                print(valeur_edit_list, "type ..",  type(valeur_edit_list))
                return render_template('t_langue/t_langue_edit.html', data=valeur_edit_list)
            else:
                # Constitution d'un dictionnaire et insertion dans la BD
                valeur_update_dictionnaire = {"value_Id_Langue": Id_Langue_edit, "value_name_langue": name_langue}

                # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
                obj_actions_t_langue = Gestiont_langue()

                # La commande MySql est envoyée à la BD
                data_Id_Langue = obj_actions_t_langue.update_langue_data(valeur_update_dictionnaire)
                # DEBUG bon marché :
                print("dataIdlangue ", data_Id_Langue, "type ", type(data_Id_Langue))
                # Message ci-après permettent de donner un sentiment rassurant aux utilisateurs.
                flash(f"Editer le langue d'un books !!!")
                # On affiche les t_langue
                return redirect(url_for('t_langue_afficher'))

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:

            print(erreur.args)
            flash(f"problème t_langue update{erreur.args[0]}")
            # En cas de problème, mais surtout en cas de non respect
            # des régles "REGEX" dans le champ "name_edit_Dispo_Langue_html" alors on renvoie le formulaire "EDIT"
            return render_template('t_langue/t_langue_edit.html', data=valeur_edit_list)

    return render_template("t_langue/t_langue_update.html")


# ---------------------------------------------------------------------------------------------------
# OM 2020.04.07 Définition d'une "route" /t_langue_select_delete , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# On change la valeur d'un langue de books par la commande MySql "UPDATE"
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/t_langue_select_delete', methods=['POST', 'GET'])
def t_langue_select_delete():

    if request.method == 'GET':
        try:

            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_t_langue = Gestiont_langue()
            # OM 2019.04.04 Récupérer la valeur de "Id_LangueDeleteHTML" du formulaire html "t_langueDelete.html"
            Id_Langue_delete = request.args.get('Id_Langue_delete_html')

            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_Id_Langue": Id_Langue_delete}


            # OM 2019.04.02 La commande MySql est envoyée à la BD
            data_Id_Langue = obj_actions_t_langue.delete_select_langue_data(valeur_delete_dictionnaire)
            flash(f"EFFACER et c'est terminé pour cette \"POV\" valeur !!!")

        except (Exception,
                pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                pymysql.IntegrityError,
                TypeError) as erreur:
            # Communiquer qu'une erreur est survenue.
            # DEBUG bon marché : Pour afficher un message dans la console.
            print(f"Erreur t_langue_delete {erreur.args[0], erreur.args[1]}")
            # C'est une erreur à signaler à l'utilisateur de cette application WEB.
            flash(f"Erreur t_langue_delete {erreur.args[0], erreur.args[1]}")

    # Envoie la page "HTML" au serveur.
    return render_template('t_langue/t_langue_delete.html', data = data_Id_Langue)


# ---------------------------------------------------------------------------------------------------
# OM 2019.04.02 Définition d'une "route" /t_langueUpdate , cela va permettre de programmer quelles actions sont réalisées avant de l'envoyer
# au navigateur par la méthode "render_template".
# Permettre à l'utilisateur de modifier un langue, et de filtrer son entrée grâce à des expressions régulières REGEXP
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route('/t_langue_delete', methods=['POST', 'GET'])
def t_langue_delete():

    # OM 2019.04.02 Pour savoir si les données d'un formulaire sont un affichage ou un envoi de donnée par des champs utilisateurs.
    if request.method == 'POST':
        try:
            # OM 2020.04.09 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
            obj_actions_t_langue = Gestiont_langue()
            # OM 2019.04.02 Récupérer la valeur de "Id_Langue" du formulaire html "t_langueAfficher.html"
            Id_Langue_delete = request.form['Id_Langue_delete_html']
            # Constitution d'un dictionnaire et insertion dans la BD
            valeur_delete_dictionnaire = {"value_Id_Langue": Id_Langue_delete}

            data_t_langue = obj_actions_t_langue.delete_langue_data(valeur_delete_dictionnaire)
            # OM 2019.04.02 On va afficher la liste des t_langue des books
            # OM 2019.04.02 Envoie la page "HTML" au serveur. On passe un message d'information dans "message_html"

            # On affiche les t_langue
            return redirect(url_for('t_langue_afficher'))



        except (pymysql.err.OperationalError, pymysql.ProgrammingError, pymysql.InternalError, pymysql.IntegrityError,
                TypeError) as erreur:
            # OM 2020.04.09 Traiter spécifiquement l'erreur MySql 1451
            # Cette erreur 1451, signifie qu'on veut effacer un "langue" de books qui est associé dans "t_books_a_langue".
            if erreur.args[0] == 1451:
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash('IMPOSSIBLE d\'effacer !!! Cette valeur est associée à des books !')
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"IMPOSSIBLE d'effacer !! Ce langue est associé à des books dans la t_books_a_langue !!! : {erreur}")
                # Afficher la liste des t_langue des books
                return redirect(url_for('t_langue_afficher'))
            else:
                # Communiquer qu'une autre erreur que la 1062 est survenue.
                # DEBUG bon marché : Pour afficher un message dans la console.
                print(f"Erreur t_langue_delete {erreur.args[0], erreur.args[1]}")
                # C'est une erreur à signaler à l'utilisateur de cette application WEB.
                flash(f"Erreur t_langue_delete {erreur.args[0], erreur.args[1]}")


            # OM 2019.04.02 Envoie la page "HTML" au serveur.
    return render_template('t_langue/t_langue_afficher.html', data=data_t_langue)