# routes_gestion_t_books_a_format.py
# MC 2020.05.05 Gestions des "routes" FLASK pour la table intermédiaire qui associe les books et les format.
from flask import render_template
from APP_FILMS import obj_mon_application


# ---------------------------------------------------------------------------------------------------
# MC 2020.05.05 Définition d'une "route" /t_books_a_format_afficher
# cela va permettre de programmer les actions avant d'interagir
# avec le navigateur par la méthode "render_template"
# Pour tester http://127.0.0.1:1234/t_books_a_format_afficher
# ---------------------------------------------------------------------------------------------------
@obj_mon_application.route("/t_books_a_format_afficher", methods=['GET', 'POST'])
def t_books_a_format_afficher():
    # # MC 2020.04.09 Pour savoir si les données d'un formulaire sont un affichage
    # # ou un envoi de donnée par des champs du formulaire HTML.
    # if request.method == "GET":
    #     try:
    #         # MC 2020.05.05 Objet contenant toutes les méthodes pour gérer (CRUD) les données.
    #         obj_actions_t_books_a_format = Gestiont_books_a_format()
    #         # Récupére les données grâce à une requête MySql définie dans la classe Gestiont_format()
    #         # Fichier data_gestion_t_format.py
    #         data_t_books_a_format = obj_actions_t_books_a_format.t_format_afficher_data()
    #         # DEBUG bon marché : Pour afficher un message dans la console.
    #
    #         # MC 2020.05.05 La ligns ci-après permet de donner un sentiment rassurant aux utilisateurs.
    #         flash("Données format de books affichées !!", "Success")
    #     except Exception as erreur:
    #         print(f"RGFG Erreur générale.")
    #         # MC 2020.05.05 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)" fichier "run_mon_app.py"
    #         # Ainsi on peut avoir un message d'erreur personnalisé.
    #         # flash(f"RGG Exception {erreur}")
    #         raise Exception(f"RGFG Erreur générale. {erreur}")

    # MC 2020.05.05 Envoie la page "HTML" au serveur.
    return render_template("t_books_a_format/t_books_a_format_afficher.html")

